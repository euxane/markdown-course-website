-- Pandoc filter which strips "solution" blocks when the environment variable
-- "STRIP_SOLUTIONS" is set, or wrap them in a block quote otherwise.
-- Useful to generate public and private handouts from the same source.
--
-- Author: Euxane TRAN-GIRARD
-- Licence: CC BY-NC 4.0

strip_solutions = os.getenv('STRIP_SOLUTIONS')

return {
  {
    Div = function(elem)
      if elem.classes:find('secret-solution') then
        if strip_solutions then
          return {}
        else
          return pandoc.BlockQuote(elem.content)
        end
      else
        return elem
      end
    end,
  }
}
